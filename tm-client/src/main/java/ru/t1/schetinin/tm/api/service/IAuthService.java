package ru.t1.schetinin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.enumerated.Role;
import ru.t1.schetinin.tm.dto.model.UserDTO;

public interface IAuthService {

    void checkRoles(@Nullable Role[] roles);

    @NotNull
    UserDTO registry(@Nullable String login, @Nullable String password, @Nullable String email);

    UserDTO check(@Nullable String login, @Nullable String password);

    void login(@Nullable String login, @Nullable String password);

    void logout();

    boolean isAuth();

    @NotNull
    String getUserId();

    @NotNull
    UserDTO getUser();

}