package ru.t1.schetinin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.schetinin.tm.api.endpoint.IUserEndpoint;
import ru.t1.schetinin.tm.command.AbstractCommand;
import ru.t1.schetinin.tm.exception.user.UserNotFoundException;
import ru.t1.schetinin.tm.dto.model.UserDTO;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    public IAuthEndpoint getAuthEndpoint() {
        return serviceLocator.getAuthEndpoint();
    }

    @NotNull
    public IUserEndpoint getUserEndpoint() {
        return serviceLocator.getUserEndpoint();
    }

    protected void showUser(@Nullable final UserDTO user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}